using SekPharmacy.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace SekPharmacy.Data
{
    public class SekPharmacyContext : IdentityDbContext<MedUser>
    {
        public DbSet<Med> Meds { get;set; }
        public DbSet<MedCategory> MedCategory { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=Med.db");
        }

        
    }
}