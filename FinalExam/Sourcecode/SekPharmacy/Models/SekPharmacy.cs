
using System; 
using System.ComponentModel.DataAnnotations; 
using Microsoft.AspNetCore.Identity;
namespace SekPharmacy.Models {

	public class MedUser : IdentityUser{ 
		public string FirstName { get; set;} 
		public string LastName { get; set;} 
	}

	public class MedCategory{
		public int MedCategoryID { get;set;}
		public string MedCatName { get;set;}
	}

	public class Med { 
		public int MedID { get; set;}
		public string MedName { get; set;} 
		public int MedCatID { get; set;}
		public MedCategory MedCat {get;set;}
		public int Price {get; set;} 
		public int Stock { get; set;}
		public string MedImage { get; set;} 
	}

	
}