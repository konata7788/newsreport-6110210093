using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SekPharmacy.Data;
using SekPharmacy.Models;

namespace SekPharmacy.Pages.StockManage
{
    public class IndexModel : PageModel
    {
        private readonly SekPharmacy.Data.SekPharmacyContext _context;

        public IndexModel(SekPharmacy.Data.SekPharmacyContext context)
        {
            _context = context;
        }

        public IList<Med> Med { get;set; }

        public async Task OnGetAsync()
        {
            Med = await _context.Meds
                .Include(m => m.MedCat).ToListAsync();
        }
    }
}
