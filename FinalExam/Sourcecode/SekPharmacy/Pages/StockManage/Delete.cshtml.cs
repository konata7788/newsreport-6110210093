using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SekPharmacy.Data;
using SekPharmacy.Models;

namespace SekPharmacy.Pages.StockManage
{
    public class DeleteModel : PageModel
    {
        private readonly SekPharmacy.Data.SekPharmacyContext _context;

        public DeleteModel(SekPharmacy.Data.SekPharmacyContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Med Med { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Med = await _context.Meds
                .Include(m => m.MedCat).FirstOrDefaultAsync(m => m.MedID == id);

            if (Med == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Med = await _context.Meds.FindAsync(id);

            if (Med != null)
            {
                _context.Meds.Remove(Med);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
