using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SekPharmacy.Data;
using SekPharmacy.Models;

namespace SekPharmacy.Pages.StockManage
{
    public class EditModel : PageModel
    {
        private readonly SekPharmacy.Data.SekPharmacyContext _context;

        public EditModel(SekPharmacy.Data.SekPharmacyContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Med Med { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Med = await _context.Meds
                .Include(m => m.MedCat).FirstOrDefaultAsync(m => m.MedID == id);

            if (Med == null)
            {
                return NotFound();
            }
           ViewData["MedCatID"] = new SelectList(_context.Set<MedCategory>(), "MedCategoryID", "MedCategoryID");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Med).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedExists(Med.MedID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MedExists(int id)
        {
            return _context.Meds.Any(e => e.MedID == id);
        }
    }
}
