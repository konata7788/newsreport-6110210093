using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SekPharmacy.Data;
using SekPharmacy.Models;

namespace SekPharmacy.Pages.MedCat
{
    public class DeleteModel : PageModel
    {
        private readonly SekPharmacy.Data.SekPharmacyContext _context;

        public DeleteModel(SekPharmacy.Data.SekPharmacyContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MedCategory MedCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MedCategory = await _context.MedCategory.FirstOrDefaultAsync(m => m.MedCategoryID == id);

            if (MedCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MedCategory = await _context.MedCategory.FindAsync(id);

            if (MedCategory != null)
            {
                _context.MedCategory.Remove(MedCategory);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
