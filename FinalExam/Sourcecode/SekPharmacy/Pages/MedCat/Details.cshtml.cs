using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SekPharmacy.Data;
using SekPharmacy.Models;

namespace SekPharmacy.Pages.MedCat
{
    public class DetailsModel : PageModel
    {
        private readonly SekPharmacy.Data.SekPharmacyContext _context;

        public DetailsModel(SekPharmacy.Data.SekPharmacyContext context)
        {
            _context = context;
        }

        public MedCategory MedCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MedCategory = await _context.MedCategory.FirstOrDefaultAsync(m => m.MedCategoryID == id);

            if (MedCategory == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
