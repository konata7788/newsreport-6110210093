using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SekPharmacy.Data;
using SekPharmacy.Models;

namespace SekPharmacy.Pages.MedCat
{
    public class EditModel : PageModel
    {
        private readonly SekPharmacy.Data.SekPharmacyContext _context;

        public EditModel(SekPharmacy.Data.SekPharmacyContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MedCategory MedCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MedCategory = await _context.MedCategory.FirstOrDefaultAsync(m => m.MedCategoryID == id);

            if (MedCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(MedCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedCategoryExists(MedCategory.MedCategoryID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MedCategoryExists(int id)
        {
            return _context.MedCategory.Any(e => e.MedCategoryID == id);
        }
    }
}
