using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SekPharmacy.Data;
using SekPharmacy.Models;

namespace SekPharmacy.Pages.MedCat
{
    public class IndexModel : PageModel
    {
        private readonly SekPharmacy.Data.SekPharmacyContext _context;

        public IndexModel(SekPharmacy.Data.SekPharmacyContext context)
        {
            _context = context;
        }

        public IList<MedCategory> MedCategory { get;set; }

        public async Task OnGetAsync()
        {
            MedCategory = await _context.MedCategory.ToListAsync();
        }
    }
}
