using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SekPharmacy.Data;
using SekPharmacy.Models;

namespace SekPharmacy.Pages.MedCat
{
    public class CreateModel : PageModel
    {
        private readonly SekPharmacy.Data.SekPharmacyContext _context;

        public CreateModel(SekPharmacy.Data.SekPharmacyContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public MedCategory MedCategory { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.MedCategory.Add(MedCategory);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}