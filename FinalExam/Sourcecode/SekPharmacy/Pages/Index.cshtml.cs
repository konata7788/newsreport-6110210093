﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SekPharmacy.Models;
using SekPharmacy.Data;

namespace SekPharmacy.Pages
{
    public class IndexModel : PageModel
    {
        private readonly SekPharmacy.Data.SekPharmacyContext _context; 
        public IndexModel(SekPharmacy.Data.SekPharmacyContext context) 
        { 
            _context = context; 
        } 
        public IList<Med> Med { get;set; } 
        public IList<MedCategory> MedCategory { get;set; }
        
        public async Task OnGetAsync() 
        { 
            Med = await _context.Meds .Include(n => n.MedCat).ToListAsync(); 
            MedCategory = await _context.MedCategory.ToListAsync();
        }
    }
}
